package classfile;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;
import java.util.stream.*;
import java.util.stream.Collectors;

import static classfile.Aron.last;
import static classfile.Aron.splitPath;

/**
<pre>
{@literal
    File Type class, add more in the future
}
{@code
    String fname = last(splitPath(fullPath));
    Pattern pat = Pattern.compile(regexFile, Pattern.CASE_INSENSITIVE);
    return pat.matcher(fname).find();

    html file
    return matchFileName(fullPath, ".\\.html$");

    Sun Apr 21 13:31:42 2019 
    Fixed error for file path: "/f.html/file.txt" 
}
</pre>
*/ 
public final class FileType{
    public static Boolean isHtml(String fullPath){
        return matchFileName(fullPath, ".\\.html$");
    }
    public static Boolean isJava(String fullPath){
        return matchFileName(fullPath, ".\\.java$");
    }
    public static Boolean isHaskell(String fullPath){
        return matchFileName(fullPath, ".\\.hs$");
    }
    public static Boolean isClang(String fullPath){
        return matchFileName(fullPath, ".\\.c$");
    }
    public static Boolean isTxt(String fullPath){
        return matchFileName(fullPath, ".\\.txt$");
    }
    public static Boolean isCpplang(String fullPath){
        return matchFileName(fullPath, ".\\.cpp$");
    }
    public static Boolean isJPG(String fullPath){
        return matchFileName(fullPath, ".\\.jpg$");
    }
    private static Boolean matchFileName(String fullPath, String regexFile){
        String fname = last(splitPath(fullPath));
        Pattern pat = Pattern.compile(regexFile, Pattern.CASE_INSENSITIVE);
        return pat.matcher(fname).find();
    }
}
