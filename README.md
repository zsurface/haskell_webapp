### haskell_webapp has moved to haskell_webapp2  Mon 23 Nov 17:46:39 2020  
###
### First Wai WebApp in Haskell
* Convert Strig to ByteString is painful 
* Convert Strig to strict ByteString to lazy ByteString are painful 

### What is Good about Haskell in Web development 
* Type safe
* Immutable variables
* You do not need test cases if you did not want to write it,
* If you want to write test cases, then there is HUnit
- It is simple to use: it comes with Haskell Framework so you do not need to install it
- Currently, I use it: HUnit-1.60.0.0 to test my own Module: AronModule.hs
- Write test cases is piece of cake:
```haskell
    import Test.HUnit
    allTests = runTestTT tests 
        where
            test1 = TestCase (assertEqual "arithmetic test" 7 (3+4)) 
            tests = TestList [
                            TestLabel "test1" test1,
                            ]

    main = do
        allTests 
        print "done" 
```
 
### What is Bad about Haskell in Web development
* There is not many tutorial and samples code online
* The library is hard to use
* Haskell is hard language
* The String is horrible:String, Bytestring, Text, Lazy, Strict..WTF
* Same function name ambiguity Problem, 
- e.g. map is in Data.List.Map and Data.Set.Map

### What is the issue?
* Send out all files from specific Dir 
* import qualified Data.ByteString.Lazy.Char8 as LB8
* LB8.pack "dog" => convert String to ByteString  

### How to run: wapp
* Compile in commnd line
```haskell
    run.sh => ./runwai 
    ghc -i/$b/haskelllib --make wai.hs -o runwai 
```
* webapp in command line
* Goto localhost:8080

### What is the URL?
* localhost:8080/up   
* localhost:8080/raw/
* localhost:8080/pdf/
* localhost:8080/upload
* -------------------------------------------------------------------------------- 
* localhost:8080/snippet
* http://localhost:8080/snippet?id=keyinput

### New Update
* Add: generate html which contains all pdf files.
* Add: send the pdf.html out

* Wed Aug 15 14:51:02 PDT 2018
* Add upload file
* Add query string
* Tue Sep 25 12:01:43 2018 
* Add prefix Map for query string as key
* Run: http://localhost:8080/snippet?id=vim
    Build (HashMap String [[String]]) prefix map from snippet file
    Pass the (HashMap String [[String]]) prefix map to Application
    Get query string from (request respond) e.g. snippet?id=vim
    Convert (query string)ByteString to String as HashMap key
    Get Value from HashMap from key above


* Tue Sep 25 19:59:29 2018 
* Integrate prefix map to HTML UI
* Add new route to Wai Server
* Add some keywords coloring

* What I have learn?
- Know more about Wai API
- Know the difference between Get and Post in Html form 
- Know more about Haskell: Text, ByteString, Lazy and Strict String
- Convert String to ByteString with 
```haskell
    pack::String->ByteString
```
- Convert ByteString to String with 
```haskell
    unpack::ByteString->String
```
- Convert String to Text with 
```haskell
    pack::String->Text 
```
- Convert Text to String with 
```haskell
    unpack::Text->String
```
- Convert Strict to Lazy with 
```haskell
    Data.Text.Lazy.toStrict::Text ->Text
```
- Convert Lazy to Strict with 
```haskell
    Data.Text.Lazy.fromStrict::Text->Text
```
## Haskell String probelm
* One of the drawback for Haskell is the String conversion problem.
- String, Strict Text, Lazy Text, Strict ByteString, lazy ByteString
### String is the least efficient that you can use. It is like String class in Java: immutable
- If you do lots of string manipulation, then you should use Text. It is similar StringBuilder or StringBuffer
- In Java StringBuilder is NOT Thread Safe, StringBuffer is Thread Safe
### Text is more efficient than String because it is mutable?
### ByetString is raw binary data
- it is like in socket level, you send(..), recv(..) data

### Wed Sep 26 15:41:50 2018 
- Color some keywords 

### Wed May 29 23:18:03 2019 
- Read and parse snippet.hs file, and conver it to HashMap
- Qeury data from Redis server for Haskell and Java lib, AronModule.hs and Aron.java
- Run shell commands in the server, it is danger!
- Add redirect URI to wai server
- Add log commands which can be autocompleted in Input Text Box

- Add delete, update and add to snippet code block with html, css and Javascript
- Use *Text.RawString.QQ* to simplify Html string, don't need to escape special char
- TODO: fix generate pdf html under /pdf/
- Added double click event to Javascript inside wai.hs file to improve user experience.

